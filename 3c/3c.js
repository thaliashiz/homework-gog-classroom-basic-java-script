const daftarVendor = {
    vendor1 : "CV. Berkat Indah",
    info1 : {
        alamat1 : "Jl. Pucuk Harum",
        telepon1 : "021-948579"
    },
    vendor2 : "PT. Jaya Abadi",
    info2 : {
        alamat2 : "Jl. Pandang Pindang",
        telepon2 : "021-933457"
    },
};
alert(`Vendor yang tersedia saat ini adalah vendor ${daftarVendor.vendor1} dengan detail alamat ${daftarVendor.info1.alamat1} dan ${daftarVendor.info1.telepon1}. 
Selain itu ada pula ${daftarVendor.vendor2} yang beralamat di ${daftarVendor.info2.alamat2} dan dapat dihubungi melalui ${daftarVendor.info2.telepon2}.`)